<?php

namespace App\Request\ParamConverter;

use App\Entity\Order;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class OrderParamConverter extends AbstractParamConverter
{
    protected $groups = ['order'];

    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() == Order::class;
    }
}
