<?php

namespace App\Request\ParamConverter;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

abstract class AbstractParamConverter implements ParamConverterInterface
{
    /** @var DenormalizerInterface */
    private $denormalizer;
    /** @var array */
    protected $allowedMethods = [Request::METHOD_POST];
    /** @var array */
    protected $groups = [];

    public function __construct(
        DenormalizerInterface $denormalizer
    ) {
        $this->denormalizer = $denormalizer;
    }

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        if (!in_array($request->getMethod(), $this->getAllowedMethods())) {
            return false;
        }

        try {
            $request->attributes->set($configuration->getName(), $this->denormalizer->denormalize(
                $this->extractContentArrayFromRequest($request),
                $configuration->getClass(),
                null,
                [
                    AbstractNormalizer::GROUPS => $this->getGroups($configuration),
                    AbstractNormalizer::OBJECT_TO_POPULATE => $this->getObjectToPopulate($request, $configuration)
                ]
            ));
        } catch (ExceptionInterface $exception) {
            throw new UnprocessableEntityHttpException($exception->getMessage());
        }

        return true;
    }

    protected function getAllowedMethods(): array
    {
        return $this->allowedMethods;
    }

    protected function extractContentArrayFromRequest(Request $request): array
    {
        return json_decode($request->getContent(), JsonEncoder::FORMAT);
    }

    protected function getGroups(ParamConverter $configuration): array
    {
        return $this->groups;
    }

    protected function getObjectToPopulate(Request $request, ParamConverter $configuration): object
    {
        $class = $configuration->getClass();

        return new $class;
    }

    abstract public function supports(ParamConverter $configuration): bool;
}
