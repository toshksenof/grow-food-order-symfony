<?php

namespace App\Controller;

use App\Entity\Order;
use App\Exception\ValidatorException;
use App\Service\OrderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route("/orders", name="order")
 */
class OrderController extends AbstractController
{
    /**
     * @Route(methods={"POST"})
     *
     * @param OrderService $orderService
     * @param Order $order
     *
     * @return JsonResponse
     *
     * @throws ValidatorException
     */
    public function create(OrderService $orderService, Order $order): JsonResponse
    {
        $orderService->save($order);

        return $this->json($order, JsonResponse::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => 'order',
        ]);
    }
}
