<?php

namespace App\Controller;

use App\Repository\PlanRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;

/**
 * @Route("/plans", name="plan")
 */
class PlanController extends AbstractController
{
    /**
     * @Route(methods={"GET"})
     *
     * @param PlanRepository $planRepository
     *
     * @return JsonResponse
     */
    public function list(PlanRepository $planRepository): JsonResponse
    {
        return $this->json($planRepository->findAll(), JsonResponse::HTTP_OK, [], [
            AbstractNormalizer::GROUPS => 'plan'
        ]);
    }
}
