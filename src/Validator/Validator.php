<?php

namespace App\Validator;

use App\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator
{
    /** @var ValidatorInterface */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param object $object
     *
     * @throws ValidatorException
     */
    public function validate(object $object): void
    {
        $errors = $this->validator->validate($object);

        if ($errors->count()) {
            throw new ValidatorException($errors);
        }
    }
}
