<?php

namespace App\Validator\Constraints;

use App\Entity\Order;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class WeekdayAllowedByPlanValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint): void
    {
        $order = $this->context->getObject();

        if (!$order instanceof Order) {
            throw new UnexpectedTypeException($value, Order::class);
        }

        foreach ($order->getPlan()->getAllowedWeekdays() as $allowedWeekday) {
            $allowedWeekdays[] = $allowedWeekday->getWeekday();
        }

        if (in_array($value, $allowedWeekdays ?? [])) {
            return;
        }

        $this->context
            ->buildViolation('Weekday isn\'t allowed by selected plan')
            ->addViolation();
    }
}
