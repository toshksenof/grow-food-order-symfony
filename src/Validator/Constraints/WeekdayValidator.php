<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class WeekdayValidator extends ConstraintValidator
{
    private const WEEKDAYS = [
        1, 2, 3, 4, 5, 6, 7,
    ];

    public function validate($value, Constraint $constraint)
    {
        if (in_array($value, self::WEEKDAYS)) {
            return;
        }

        $this->context
            ->buildViolation('Value is not valid weekday')
            ->addViolation();
    }
}
