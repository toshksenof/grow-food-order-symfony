<?php

namespace App\Normalizer;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Exception\ValidatorException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class UserNormalizer extends ObjectNormalizer
{
    /** @var UserService */
    private $userService;
    /** @var UserRepository */
    private $userRepository;

    public function __construct(
        UserService $addressService,
        UserRepository $addressRepository,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null
    ) {
        parent::__construct($classMetadataFactory, $nameConverter, $propertyAccessor, $propertyTypeExtractor);
        $this->userService = $addressService;
        $this->userRepository = $addressRepository;
    }

    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return count($data) == 1 && isset($data['phone']) && $type == User::class;
    }

    /**
     * @param $data
     * @param $class
     * @param null $format
     * @param array $context
     *
     * @return User
     *
     * @throws ValidatorException
     * @throws ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = []): User
    {
        if (!$user =  $this->userRepository->findOneByPhone($data['phone'])) {
            /** @var User $user */
            $user = parent::denormalize($data, $class, $format, $context);
            $this->userService->save($user);
        }

        return $user;
    }

    public function supportsNormalization($data, $format = null)
    {
        return false;
    }
}
