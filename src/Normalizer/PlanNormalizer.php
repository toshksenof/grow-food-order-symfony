<?php

namespace App\Normalizer;

use App\Entity\AllowedWeekday;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PlanNormalizer extends ObjectNormalizer
{
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof AllowedWeekday;
    }

    public function normalize($object, $format = null, array $context = []): ?int
    {
        if (!$object instanceof AllowedWeekday) {
            return null;
        }

        return $object->getWeekday();
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return false;
    }
}
