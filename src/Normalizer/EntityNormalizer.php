<?php

namespace App\Normalizer;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class EntityNormalizer extends ObjectNormalizer
{
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null
    ) {
        parent::__construct($classMetadataFactory, $nameConverter, $propertyAccessor, $propertyTypeExtractor);
        $this->entityManager = $entityManager;
    }

    public function supportsDenormalization($data, $type, $format = null, $context = null): bool
    {
        return count($data) == 1 && isset($data['id']) && (strpos($type, 'App\\Entity\\') === 0);
    }

    /**
     * @param $data
     * @param $class
     * @param null $format
     * @param array $context
     *
     * @return object
     * @throws EntityNotFoundException
     */
    public function denormalize($data, $class, $format = null, array $context = []): object
    {
        if (!$object = $this->entityManager->find($class, $data['id'])) {
            throw new EntityNotFoundException("$class not found with id {$data['id']}");
        }

        return $object;
    }

    public function supportsNormalization($data, $format = null)
    {
        return false;
    }
}
