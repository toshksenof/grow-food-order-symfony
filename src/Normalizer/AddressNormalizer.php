<?php

namespace App\Normalizer;

use App\Entity\Address;
use App\Repository\AddressRepository;
use App\Repository\UserRepository;
use App\Service\AddressService;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\PropertyTypeExtractorInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactoryInterface;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Exception\ValidatorException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

class AddressNormalizer extends ObjectNormalizer
{
    /** @var AddressService */
    private $addressService;
    /** @var AddressRepository */
    private $addressRepository;
    /** @var UserRepository */
    private $userRepository;

    public function __construct(
        AddressService $addressService,
        AddressRepository $addressRepository,
        UserRepository $userRepository,
        ?ClassMetadataFactoryInterface $classMetadataFactory = null,
        ?NameConverterInterface $nameConverter = null,
        ?PropertyAccessorInterface $propertyAccessor = null,
        ?PropertyTypeExtractorInterface $propertyTypeExtractor = null
    ) {
        parent::__construct($classMetadataFactory, $nameConverter, $propertyAccessor, $propertyTypeExtractor);
        $this->addressService = $addressService;
        $this->addressRepository = $addressRepository;
        $this->userRepository = $userRepository;
    }

    public function supportsDenormalization($data, $type, $format = null): bool
    {
        return $type == Address::class && isset(
                $data['user']['phone'],
                $data['firstLine'],
                $data['postcode'],
                $data['city']
            );
    }

    /**
     * @param $data
     * @param $class
     * @param null $format
     * @param array $context
     *
     * @return Address
     * @throws ValidatorException
     * @throws ExceptionInterface
     */
    public function denormalize($data, $class, $format = null, array $context = []): Address
    {
        if (($dataToFind = $this->extractDataToFind($data))
            && $address = $this->addressRepository->findOneBy($dataToFind)) {
            return $address;
        }
        /** @var Address $address */
        $address = parent::denormalize($data, $class, $format);
        $this->addressService->save($address);

        return $address;
    }

    private function extractDataToFind(array $data): array
    {
        if (!$user = $this->userRepository->findOneByPhone($data['user']['phone'])) {
            return [];
        }

        return [
            'firstLine' => $data['firstLine'],
            'secondLine' => $data['secondLine'] ?? null,
            'postcode' => $data['postcode'],
            'city' => $data['city'],
            'user' => [
                'id' => $user ? $user->getId() : null,
            ],
        ];
    }

    public function supportsNormalization($data, $format = null)
    {
        return false;
    }
}
