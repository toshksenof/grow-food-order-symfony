<?php

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime|null
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime|null
     */
    protected $updatedAt;

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamps(): void
    {
        $dateTimeNow = new \DateTime();
        $this->updatedAt = $dateTimeNow;
        if ($this->getCreatedAt() === null) {
            $this->createdAt = $dateTimeNow;
        }
    }

    public function getCreatedAt() :?\DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt() :?\DateTime
    {
        return $this->updatedAt;
    }
}
