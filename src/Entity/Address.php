<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"order"})
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"order"})
     *
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $firstLine;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Groups({"order"})
     *
     * @var string|null
     */
    private $secondLine;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"order"})
     *
     * @Assert\NotBlank()
     *
     * @var string
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=8, options={})
     *
     * @Groups({"order"})
     *
     * @Assert\NotBlank()
     * @Assert\Length(min="6", max="8")
     * @Assert\Regex(pattern="/^[0-9]+$/", message="Only numbers allowed")
     *
     * @var string
     */
    private $postcode;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     *
     * @Groups({"order"})
     *
     * @Assert\Valid()
     *
     * @var User
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstLine(): string
    {
        return $this->firstLine;
    }

    public function setFirstLine(string $firstLine): self
    {
        $this->firstLine = $firstLine;

        return $this;
    }

    public function getSecondLine(): ?string
    {
        return $this->secondLine;
    }

    public function setSecondLine(?string $secondLine): self
    {
        $this->secondLine = $secondLine;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPostcode(): string
    {
        return $this->postcode;
    }

    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
