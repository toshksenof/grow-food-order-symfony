<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @Groups({"plan", "order"})
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     *
     * @Groups({"plan"})
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="AllowedWeekday", mappedBy="plan")
     *
     * @Groups({"plan"})
     *
     * @var ArrayCollection|AllowedWeekday[]
     */
    private $allowedWeekdays;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     *
     * @Groups({"plan"})
     *
     * @var float
     */
    private $price;

    public function __construct()
    {
        $this->allowedWeekdays = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return AllowedWeekday[]
     */
    public function getAllowedWeekdays()
    {
        return $this->allowedWeekdays;
    }

    public function addAllowedWeekday(AllowedWeekday $allowedWeekday): self
    {
        if (!$this->allowedWeekdays->contains($allowedWeekday)) {
            $this->allowedWeekdays->add($allowedWeekday);
            $allowedWeekday->setPlan($this);
        }

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
