<?php

namespace App\Entity;

use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=11, unique=true, options={"fixed" = true})
     *
     * @Groups({"order"})
     *
     * @Assert\Length(11)
     * @Assert\Regex(pattern="/^[0-9]+$/", message="Only numbers allowed")
     *
     * @var string
     */
    private $phone;

    /**
     * @ORM\OneToMany(targetEntity="Address", mappedBy="user")
     *
     * @var Address[]|ArrayCollection
     */
    private $addresses;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }
}
