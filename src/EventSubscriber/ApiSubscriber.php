<?php

namespace App\EventSubscriber;

use App\Exception\ValidatorException;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiSubscriber implements EventSubscriberInterface
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getException();

        if ($exception instanceof ValidatorException) {
            $response = $this->getResponseForValidatorException($exception);
        } elseif ($exception instanceof HttpException) {
            $response = $this->getResponseForHttpException($exception);
        } elseif ($exception instanceof EntityNotFoundException) {
            $response = $this->getResponseForEntityNotFoundException($exception);
        }

        $event->setResponse(
            $response ?? $this->createResponse(
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                ['server' => 'Internal server error']
            )
        );
    }

    private function getResponseForValidatorException(ValidatorException $validatorException): JsonResponse
    {
        $messages = [];
        foreach ($validatorException->getConstraintViolationList() as $violation) {
            $messages[$violation->getPropertyPath()][] = $violation->getMessage();
        }

        return $this->createResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, $messages);
    }

    private function getResponseForHttpException(HttpException $httpException): JsonResponse
    {
        return $this->createResponse(
            $httpException->getStatusCode(),
            ['message' => $httpException->getMessage()]
        );
    }

    private function getResponseForEntityNotFoundException(
        EntityNotFoundException $entityNotFoundException
    ): JsonResponse {
        return $this->createResponse(
            JsonResponse::HTTP_NOT_FOUND,
            $entityNotFoundException->getMessage()
        );
    }

    private function createResponse($code, $messages): JsonResponse
    {
        return new JsonResponse(compact('code', 'messages'), $code);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
        ];
    }
}