<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190629112753 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create plan tables';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('CREATE TABLE allowed_weekday (
          id INT AUTO_INCREMENT NOT NULL, 
          plan_id INT NOT NULL, 
          weekday SMALLINT NOT NULL, 
          INDEX IDX_FF0DA243E899029B (plan_id), 
          UNIQUE INDEX unique_allowed_weekday (plan_id, weekday), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('CREATE TABLE plan (
          id INT AUTO_INCREMENT NOT NULL, 
          name VARCHAR(255) NOT NULL, 
          price NUMERIC(10, 2) NOT NULL, 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');

        $this->addSql('ALTER TABLE 
          allowed_weekday 
        ADD 
          CONSTRAINT FK_FF0DA243E899029B FOREIGN KEY (plan_id) REFERENCES plan (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE allowed_weekday DROP FOREIGN KEY FK_FF0DA243E899029B');
        $this->addSql('DROP TABLE allowed_weekday');
        $this->addSql('DROP TABLE plan');
    }
}
