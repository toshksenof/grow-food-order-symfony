<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190629125610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Create order';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('CREATE TABLE `order` (
          id INT AUTO_INCREMENT NOT NULL, 
          plan_id INT NOT NULL, 
          address_id INT DEFAULT NULL, 
          user_id INT NOT NULL, 
          username VARCHAR(255) NOT NULL, 
          weekday SMALLINT NOT NULL, 
          created_at DATETIME NOT NULL, 
          updated_at DATETIME NOT NULL, 
          INDEX IDX_F5299398E899029B (plan_id), 
          INDEX IDX_F5299398F5B7AF75 (address_id), 
          INDEX IDX_F5299398A76ED395 (user_id), 
          PRIMARY KEY(id)
        ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE 
          `order` 
        ADD 
          CONSTRAINT FK_F5299398E899029B FOREIGN KEY (plan_id) REFERENCES plan (id)');
        $this->addSql('ALTER TABLE 
          `order` 
        ADD 
          CONSTRAINT FK_F5299398F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id)');
        $this->addSql('ALTER TABLE 
          `order` 
        ADD 
          CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DROP TABLE `order`');
    }
}
