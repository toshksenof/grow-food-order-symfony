<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190629113000 extends AbstractMigration
{
    /** @var array */
    const PLANS = [
        [1, 'Great plan, Walter', [1,3,5]],
        [2, 'Ingenious plan, if I understand correctly', [2,4,6]],
    ];

    public function getDescription(): string
    {
        return 'Fill database with default plans';
    }

    public function up(Schema $schema): void
    {
        foreach (self::PLANS as $plan) {
            list($id, $name, $allowedDays) = $plan;
            $price = rand(3999, 5999);
            $this->addSql("
                INSERT INTO plan(id, name, price) VALUES ($id, '$name', $price)
            ");
            foreach ($allowedDays as $allowedDay) {
                $this->addSql("
                    INSERT INTO allowed_weekday(plan_id, weekday) VALUES ($id, $allowedDay)
                ");
            }
        }
    }

    public function down(Schema $schema): void
    {
        // not needed
    }
}
