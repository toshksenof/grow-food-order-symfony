<?php

namespace App\Service;

use App\Entity\User;
use App\Exception\ValidatorException;

class UserService extends AbstractEntityService
{
    /**
     * @param User $user
     *
     * @throws ValidatorException
     */
    public function save(User $user): void
    {
        $this->doSave($user);
    }
}
