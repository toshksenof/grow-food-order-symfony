<?php

namespace App\Service;

use App\Entity\Address;
use App\Exception\ValidatorException;

class AddressService extends AbstractEntityService
{
    /**
     * @param Address $address
     *
     * @throws ValidatorException
     */
    public function save(Address $address): void
    {
        $this->doSave($address);
    }
}
