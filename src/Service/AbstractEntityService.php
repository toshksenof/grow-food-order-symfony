<?php

namespace App\Service;

use App\Validator\Validator;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\ValidatorException;

abstract class AbstractEntityService
{
    /** @var Validator */
    private $validator;
    /** @var EntityManagerInterface */
    private $entityManager;

    public function __construct(
        Validator $validator,
        EntityManagerInterface $entityManager
    ) {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $object
     *
     * @throws ValidatorException
     */
    protected function doSave(object $object): void
    {
        $this->validator->validate($object);
        $this->entityManager->persist($object);
        $this->entityManager->flush();
    }
}
