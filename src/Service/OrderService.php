<?php

namespace App\Service;

use App\Entity\Order;
use App\Exception\ValidatorException;

class OrderService extends AbstractEntityService
{
    /**
     * @param Order $order
     *
     * @throws ValidatorException
     */
    public function save(Order $order): void
    {
        $order->setUser($order->getAddress()->getUser());
        $this->doSave($order);
    }
}
